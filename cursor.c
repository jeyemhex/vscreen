/*======[ CURSOR ]======
 *
 * Description: A test program for VScreen, drawing a single pixel at the
 *              current mouse location
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */

# include "vscreen.h"
// A simple program to put a pixel in the middle of a screen
int main(int argc, char **argv) {

    // Initialise VScreen and get a new instance
    vscreen_init();

    int resolution[2]={160,90}; // Create a 160x90 virtual screen
    int scale=10;              // Make each virtual pixel 8x8 pixels large
    int fullscreen=1;        // Do not make fullscreen

    VScreen *vscreen = vscreen_new("One White Pixel", resolution, scale, fullscreen);

    int last_cursor[2] = {-1,-1};

    // Start main loop
    while (vscreen_open_screens() > 0) {

        // Overwrite the previous cursor location with black
        if((last_cursor[0] >= 0) && (last_cursor[1] >= 0)) {
            vscreen->buffer[last_cursor[0]][last_cursor[1]]= 0;
        }

        // Write a white pixel at the current cursor location
        if((vscreen->cursor[0] >= 0) && (vscreen->cursor[1] >= 0)) {
            if(vscreen->click[0]) {
                vscreen->buffer[vscreen->cursor[0]][vscreen->cursor[1]] = 255;
            } else {
                vscreen->buffer[vscreen->cursor[0]][vscreen->cursor[1]] = 127;
            }
        }

        last_cursor[0] = vscreen->cursor[0];
        last_cursor[1] = vscreen->cursor[1];

        // Update the VScreen
        vscreen_update(vscreen);

        // Wait 1/60th of a second: the poor mans frame limiter!
        usleep(1e6/60);
    }

    // Clean up and go home
    vscreen_finalise();
    return 0;
}

