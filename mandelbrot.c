/*======[ FOURIER ]======
 *
 * Description: A test program for VScreen, drawing a Fourier pattern
 *
 * Author: Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "vscreen.h"

#include <complex.h>

struct Mand {
    int recalc;
    double xmin, xmax, ymin, ymax;
    int click1[2], click2[2];
    int prev_cursor[3];
    int prev_click;
    int prev_pix;
};

int* pixel(double x,double y, int res[2],struct Mand m) {
    int* p = malloc(2*sizeof(int));
    p[0] = res[0] * (x - m.xmin) / (m.xmax-m.xmin);
    p[1] = res[1] * (y - m.ymin) / (m.ymax-m.ymin);

    return p;
}

double *coord(int i,double j, int res[2],struct Mand m) {
    double *c = malloc(2*sizeof(int));
    c[0] = i * (m.xmax-m.xmin)/res[0] + m.xmin;
    c[1] = j * (m.ymax-m.ymin)/res[1] + m.ymin;

    return c;
}

// A pretty-looking Fourier grid that gently wafts through time
void update_buffer (VScreen *vs, struct Mand *m) {

    if(!m->recalc) {
        if (vs->click[0]) {
            if(m->prev_click != 0) {
                double xrange = (m->xmax - m->xmin)/2;
                double yrange = (m->ymax - m->ymin)/2;

                double *c = coord(vs->cursor[0], vs->cursor[1], vs->resolution, *m);

                m->xmin = c[0]-xrange/2;
                m->xmax = c[0]+xrange/2;
                m->ymin = c[1]-yrange/2;
                m->ymax = c[1]+yrange/2;
                fprintf (stderr,"New range: [%e:%e] [%e:%e]\n", m->xmin,m->xmax,m->ymin,m->ymax);
                m->recalc = 1;
                for (int i=0;i<vs->resolution[0];i++) {
                    for (int j=0;j<vs->resolution[1];j++) {
                        vs->buffer[i][j] = 0;
                    }
                }
            }
            m->prev_click = 0;
        } else if (vs->click[1]) {
            if(m->prev_click != 1) {
                double xrange = (m->xmax - m->xmin)*2;
                double yrange = (m->ymax - m->ymin)*2;

                double *c = coord(vs->cursor[0], vs->cursor[1], vs->resolution, *m);

                m->xmin = c[0]-xrange/2;
                m->xmax = c[0]+xrange/2;
                m->ymin = c[1]-yrange/2;
                m->ymax = c[1]+yrange/2;

                m->recalc = 1;
                for (int i=0;i<vs->resolution[0];i++) {
                    for (int j=0;j<vs->resolution[1];j++) {
                        vs->buffer[i][j] = 0;
                    }
                }
            }
            m->prev_click = 1;
        } else { 
            m->prev_click = -1;
        }

    } else {
        for(double x=m->xmin; x<m->xmax; x+=(m->xmax-m->xmin)/vs->resolution[0]) {
            for(double y=m->ymin; y<m->ymax; y+=(m->ymax-m->ymin)/vs->resolution[1]) {

                double xi=0.0;
                double yi=0.0;
                int i;
                for (i=0;i<=255; i++) {
                    if (xi*xi + yi*yi >= 2*2) break;
                    double xtmp = xi*xi - yi*yi + x;
                    yi = 2*xi*yi + y;
                    xi = xtmp;
                }

                int* p = pixel(x,y, vs->resolution, *m);
                if(p[0] < vs->resolution[0] && p[1] < vs->resolution[1] &&
                        p[0] >= 0 && p[1] >= 0) {
                    vs->buffer[p[0]][p[1]] = i>255 ? 0 : 255-i;
                }
                free(p);
            }
        }
        m->recalc = 0;
    }

    vs->buffer[m->prev_cursor[0]][m->prev_cursor[1]] = m->prev_cursor[2];

    m->prev_cursor[2] = vs->buffer[vs->cursor[0]][vs->cursor[1]];
    for(int i=0;i<2;i++) m->prev_cursor[i] = vs->cursor[i];

    vs->buffer[vs->cursor[0]][vs->cursor[1]] = vs->buffer[vs->cursor[0]][vs->cursor[1]] > 128 ? 0 : 255;
}

int main(int argc, char **argv) {

    // Initialise VScreen and get a new instance
    vscreen_init();

    struct Mand m;
    m.recalc=1;
    m.xmin = -2.5;
    m.xmax =  1.0;
    m.ymin = -1.0;
    m.ymax =  1.0;
    m.prev_cursor[0]= 1;
    m.prev_cursor[1]= 1;
    m.prev_click=0;

    int resolution[2]={1400,800}, scale=1, fullscreen=0;
    VScreen *vscreen = vscreen_new("Mandelbrot set", resolution, scale, fullscreen);

    // Start main loop
    while (vscreen_open_screens() > 0) {


        // Calculate the grid to display
        update_buffer(vscreen, &m);

        // Update the VScreen
        vscreen_update(vscreen);

        // Wait 1/60th of a second: the poor mans frame limiter!
        usleep(1e6/24);
    }

    // Clean up and go home
    vscreen_finalise();
    return 0;
}
