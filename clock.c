/*======[ PIXEL ]======
 *
 * Description: A test program for VScreen, drawing a single pixel
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */

#include <time.h>
#include "vscreen.h"
#include <GLFW/glfw3.h>


// A simple program to put a pixel in the middle of a screen
int main(int argc, char **argv) {

    // Initialise VScreen and get a new instance
    vscreen_init();
    glfwWindowHint(GLFW_DECORATED, 0);

    int resolution[2]={16,8}; // Create a 64x64 virtual screen
    int scale=17;             // Make each virtual pixel 8x8 pixels large
    int fullscreen=0;        // Do not make fullscreen

    VScreen *vscreen = vscreen_new("LED clock", resolution, scale, fullscreen);

    int click, prev_click;
    int y_sec = 0;
    int y_clk = 1;
    int show_date = 0;
    // Start main loop
    while (vscreen_open_screens() > 0) {
        int pos[2];

        time_t now = time(0);
        struct tm *t = localtime(&now);

        click = vscreen->click[0];

        if ((click == 1) && (prev_click == 0)) {
            show_date = vscreen->time;
            vscreen->force_redraw = 1;
        }

        char ch[4];
        if ((vscreen->time - show_date) <= 5) {
            int day = t->tm_wday;
            switch (day) {
                  case 0: {
                    ch[0] = 'S'; ch[1] = 'u'; break;
                } case 1: {
                    ch[0] = 'M'; ch[1] = 'o'; break;
                } case 2: {
                    ch[0] = 'T'; ch[1] = 'u'; break;
                } case 3: {
                    ch[0] = 'W'; ch[1] = 'e'; break;
                } case 4: {
                    ch[0] = 'T'; ch[1] = 'h'; break;
                } case 5: {
                    ch[0] = 'F'; ch[1] = 'r'; break;
                } case 6: {
                    ch[0] = 'S'; ch[1] = 'a'; break;
                }
            }

            ch[2] = t->tm_mday / 10  + 48;
            ch[3] = t->tm_mday % 10  + 48;

        } else {
            ch[0] = t->tm_hour / 10 + 48;
            ch[1] = t->tm_hour % 10 + 48;
            ch[2] = t->tm_min / 10  + 48;
            ch[3] = t->tm_min % 10  + 48;
        }

        pos[0]=0;pos[1]=y_clk; vscreen_write_char(vscreen, ch[0], pos);
        pos[0]=4;pos[1]=y_clk; vscreen_write_char(vscreen, ch[1], pos);
        pos[0]=9;pos[1]=y_clk; vscreen_write_char(vscreen, ch[2], pos);
        pos[0]=13;pos[1]=y_clk; vscreen_write_char(vscreen,ch[3], pos);


        if (t->tm_sec == 0) {
            for(int i=0;i<16;i++) vscreen->buffer[i][y_sec] = 0;
        }
        vscreen->buffer[t->tm_sec/4][y_sec] = 255;


        // Update the VScreen
        vscreen_update(vscreen);

        prev_click = click;
        // Wait 1/60th of a second: the poor mans frame limiter!
        usleep(1e6/4);
    }

    // Clean up and go home
    vscreen_finalise();
    return 0;
}


