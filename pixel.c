/*======[ PIXEL ]======
 *
 * Description: A test program for VScreen, drawing a single pixel
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */

# include "vscreen.h"
// A simple program to put a pixel in the middle of a screen
int main(int argc, char **argv) {

    // Initialise VScreen and get a new instance
    vscreen_init();

    int resolution[2]={8,8}; // Create a 64x64 virtual screen
    int scale=32;              // Make each virtual pixel 8x8 pixels large
    int fullscreen=0;        // Do not make fullscreen

    VScreen *vscreen = vscreen_new("One White Pixel", resolution, scale, fullscreen);

    // Start main loop
    while (vscreen_open_screens() > 0) {

        // Display a white pixel in the middle
        for (int i=0;i<8;i++) for (int j=0;j<8;j++) vscreen->buffer[i][j] = 255;

        // Update the VScreen
        vscreen_update(vscreen);

        // Wait 1/60th of a second: the poor mans frame limiter!
        usleep(1e6/24);
    }

    // Clean up and go home
    vscreen_finalise();
    return 0;
}

