/*
 * vscreen.h
 * Copyright (C) 2017 Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef VSCREEN_H
#define VSCREEN_H

#include <GLFW/glfw3.h>
#include <stdio.h>

enum Pixel_t {CIRCLE, SQUARE};

typedef struct vscreen {
  char *name;              // The name of the screen
  int resolution[2];       // Virtual screen resolution
  int scale;               // Initial scale of the screen

  unsigned char **buffer;  // Screen buffer
  float time;              // Time since the screen was opened

  unsigned char **screen;  // What's actually on the screen
  float start_time;        // When the screen was opened

  int cursor[2];           // Cursor postion
  int click[2];

  enum Pixel_t pixel_type;

  GLFWwindow *window;      // The actual GLFW window we're drawing in
  int size[2];             // The GLFW window size

  int force_redraw;

} VScreen;


// Initialise the VScreen environment
void vscreen_init(void);

// Create a new VScreen instance
VScreen *vscreen_new(char *name, int resolution[2], int scale, int fullscreen);

// Update the VScreen with its current buffer
void vscreen_update(VScreen *vs);

// Clears the current screen
void vscreen_clear(VScreen *vs);

// Close a VScreen
void vscreen_close(VScreen *vs);

// Where is the cursor?
void vscreen_get_cursor(VScreen *vs);

// How many open VScreen windows are there?
int vscreen_open_screens(void);

// Finalise the VScreen environment
void vscreen_finalise(void);

// Write a character to a position
void vscreen_write_char(VScreen *vs, char c, int pos[2]);

// Write a string to a position
void vscreen_write_string(VScreen *vs, char *str, int len, int pos[2]);

// This is in here to get rid of compilation warnings, it's just usleep really
extern int usleep(__useconds_t);

#endif /* !VSCREEN_H */
