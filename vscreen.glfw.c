/*======[ vscreen ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <GLFW/glfw3.h>
#include <math.h>

#include "vscreen.h"
#define PI 3.14159265

int open_screens; // So we can keep track of how many screens are open

// For GLFW, to report errors I guess...
static void error_callback(int error, const char* description) {
    fprintf(stderr, "Error: %s\n", description);
}

// How to deal with keyboard interupts
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {

//EJH//     // If escape is pressed, close the window
//EJH//     if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
//EJH//         glfwSetWindowShouldClose(window, GLFW_TRUE);
}

// Initialise the VScreen environment, mostly GLFW boilerplate stuff
void vscreen_init(void) {
    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        exit(1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    open_screens = 0;
}

// Create a new VScreen instance
VScreen *vscreen_new(char *name, int res[2], int scale, int fullscreen) {

    // Allocate new vscreen
    VScreen *vs;
    vs = malloc(sizeof(VScreen));

    // Create glfw window
    GLFWmonitor *f = NULL;
    if(fullscreen) f = glfwGetPrimaryMonitor();

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    vs->window = glfwCreateWindow( scale*res[0], scale*res[1], name, f, NULL);
    if(!vs->window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    // More GLFW boilerplate
    glfwMakeContextCurrent(vs->window);

    glfwSetKeyCallback(vs->window, key_callback);

    glfwSwapInterval(1);

    // Copy across the arguments into the instance
    vs->name = name;
    for(int i=0;i<2;i++) vs->resolution[i] = res[i];
    vs->scale = scale;

    // Allocate and zero the screen buffer
    vs->buffer = malloc(res[0]*sizeof(char*));
    for (int i=0; i<res[0]; i++) {
        vs->buffer[i] = malloc(res[1]*sizeof(char));
        for(int j=0; j<res[1]; j++) {
            vs->buffer[i][j] = 0;
        }
    }

    // Allocate and zero the screen
    vs->screen = malloc(res[0]*sizeof(char*));
    for (int i=0; i<res[0]; i++) {
        vs->screen[i] = malloc(res[1]*sizeof(char));
        for(int j=0; j<res[1]; j++) {
            vs->screen[i][j] = 0;
        }
    }

    glfwSetInputMode(vs->window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    vscreen_get_cursor(vs);

    // Get the current time, and set the open time to 0
    vs->start_time = glfwGetTime();
    vs->time = 0;
    vs->pixel_type = CIRCLE;

    // We've successfully opened a new screen! =)
    open_screens++;

    vs->force_redraw = 0;

    return vs;
}

// Update the VScreen with its current buffer
void vscreen_update(VScreen *vs) {

    // Tell GLFW we're updating this screen, and deal with any resizes
    glfwMakeContextCurrent(vs->window);

    int height, width;
    int force_redraw = vs->force_redraw;
    glfwGetFramebufferSize(vs->window, &width, &height);

    if ((height != vs->size[0]) || (width != vs->size[1])) {
        vs->size[0] = height;
        vs->size[1] = width;
        force_redraw = 1;
    }

    // Set the GL viewport to the current window and draw our pixels
    glViewport(0, 0, width, height);

    switch (vs->pixel_type) {
        case SQUARE:
            for(int i = 0; i < vs->resolution[0]; i++) {
                for(int j = 0;j < vs->resolution[1]; j++) {

                    // only draw if the buffer pixel actually changed
                    if ((vs->buffer[i][j] != vs->screen[i][j]) || force_redraw) {

                        // update the screen pixel
                        vs->screen[i][j] = vs->buffer[i][j];

                        // Draw the pixel coloured by the screen value (0-255)
                        glBegin(GL_QUADS);
                            glColor3f(vs->screen[i][j]/255., vs->screen[i][j]/255., vs->screen[i][j]/255.);
                            glVertex2f((2.0*i)/vs->resolution[0]   - 1,   (2.0*j)/vs->resolution[1]     - 1);
                            glColor3f(vs->screen[i][j]/255., vs->screen[i][j]/255., vs->screen[i][j]/255.);
                            glVertex2f((2.0*i)/vs->resolution[0]   - 1,   (2.0*(j+1))/vs->resolution[1] - 1);
                            glColor3f(vs->screen[i][j]/255., vs->screen[i][j]/255., vs->screen[i][j]/255.);
                            glVertex2f((2.0*(i+1))/vs->resolution[0] - 1, (2.0*(j+1))/vs->resolution[1] - 1);
                            glColor3f(vs->screen[i][j]/255., vs->screen[i][j]/255., vs->screen[i][j]/255.);
                            glVertex2f((2.0*(i+1))/vs->resolution[0] - 1, (2.0*j)/vs->resolution[1]     - 1);
                        glEnd();
                    }
                }
            } 
            break;
        case CIRCLE:
            for(int i = 0; i < vs->resolution[0]; i++) {
                for(int j = 0;j < vs->resolution[1]; j++) {

                    // only draw if the buffer pixel actually changed
                    if ((vs->buffer[i][j] != vs->screen[i][j]) || force_redraw) {

                        // update the screen pixel
                        vs->screen[i][j] = vs->buffer[i][j];
                        float mid[2] = {(2.0*(i+0.5))/vs->resolution[0]   - 1,   (2.0*(j+0.5))/vs->resolution[1]     - 1};
                        float r[2] = {0.8/vs->resolution[0],0.8/vs->resolution[1]};
                        glBegin(GL_POLYGON);
                            for (float theta=0; theta < 2*PI; theta+=PI/180) {
                                glColor3f((vs->screen[i][j] + 45)/300., 0,0);
                                glVertex2f(mid[0]+r[0]*cos(theta), mid[1]+r[1]*sin(theta));
                            }
                        glEnd();
                    }
                }
            }
            break;
    }

    // Swap GLFW buffers
    glfwSwapBuffers(vs->window);

    // Have any keys been pressed?
    glfwPollEvents();

    vs->force_redraw = 0;

    vscreen_get_cursor(vs);

    // Now's as good a time as any to update the instance time
    vs->time = glfwGetTime() - vs->start_time;

    // Find out if the screen should close
    if (glfwWindowShouldClose(vs->window)) {
        vscreen_close(vs);
    }
}

void vscreen_clear(VScreen *vs) {
    for (int i=0; i < vs->resolution[0]; i++) {
        for (int j=0; j < vs->resolution[0]; j++) {
            vs->screen[i][j] = 1;
            vs->buffer[i][j] = 0;
        }
    }
    vscreen_update(vs);
}

// Get the cursor position
void vscreen_get_cursor(VScreen *vs) {
    double x,y;
    glfwGetCursorPos(vs->window, &x, &y);

    vs->cursor[0] = (int) (x/vs->scale);
    vs->cursor[1] = vs->resolution[1]- ((int) (y/vs->scale) + 1);

    if((vs->cursor[0] >= vs->resolution[0]) || (vs->cursor[0] < 0)) vs->cursor[0] = -1;
    if((vs->cursor[1] >= vs->resolution[1]) || (vs->cursor[1] < 0)) vs->cursor[1] = -1;

    for(int i=0;i<2;i++) {
        int click = glfwGetMouseButton(vs->window, i);
        vs->click[i] = click == GLFW_PRESS ? 1 : 0;
    }
}

// Close the VScreen
void  vscreen_close(VScreen *vs) {
    glfwDestroyWindow(vs->window);
    open_screens--;
}

// How many open VScreen windows are there?
int vscreen_open_screens(void) { return open_screens; }

// Finalise the VScreen environment
void vscreen_finalise(void) {
    glfwTerminate();
}
