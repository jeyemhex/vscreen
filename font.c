/*======[ PIXEL ]======
 *
 * Description: A test program for VScreen, drawing a single pixel
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */

#include <time.h>
# include "vscreen.h"

// A simple program to put a pixel in the middle of a screen
int main(int argc, char **argv) {

    // Initialise VScreen and get a new instance
    vscreen_init();

    int resolution[2]={128,24}; // Create a 64x64 virtual screen
    int scale=8;              // Make each virtual pixel 8x8 pixels large
    int fullscreen=0;        // Do not make fullscreen

    VScreen *vscreen = vscreen_new("One White Pixel", resolution, scale, fullscreen);

    // Start main loop
    while (vscreen_open_screens() > 0) {
        int pos[2] = {0, 17};

        vscreen_write_string(vscreen, " !\"#$%&'()*+,-./0123456789:;<=>?", 32, pos);
        pos[1] -= 8;
        vscreen_write_string(vscreen, "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_", 32, pos);
        pos[1] -= 8;
        vscreen_write_string(vscreen, "`abcdefghijklmnopqrstuvwxyz{|}~", 32, pos);

        // Update the VScreen
        vscreen_update(vscreen);

        // Wait 1/60th of a second: the poor mans frame limiter!
        usleep(1e6/24);
    }

    // Clean up and go home
    vscreen_finalise();
    return 0;
}


