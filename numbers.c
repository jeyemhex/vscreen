/*======[ PIXEL ]======
 *
 * Description: A test program for VScreen, drawing a single pixel
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */

#include <time.h>
# include "vscreen.h"

// A simple program to put a pixel in the middle of a screen
int main(int argc, char **argv) {

    // Initialise VScreen and get a new instance
    vscreen_init();

    int resolution[2]={40,8}; // Create a 64x64 virtual screen
    int scale=32;              // Make each virtual pixel 8x8 pixels large
    int fullscreen=0;        // Do not make fullscreen

    VScreen *vscreen = vscreen_new("One White Pixel", resolution, scale, fullscreen);

    int i = 0;
    // Start main loop
    while (vscreen_open_screens() > 0) {
        int pos[2];

        pos[0]=(i - 4) % 44 - 4;pos[1]=1; vscreen_write_char(vscreen,  ' ', pos);
        pos[0]=(i + 0) % 44 - 4;pos[1]=1; vscreen_write_char(vscreen,  '0', pos);
        pos[0]=(i + 4) % 44 - 4;pos[1]=1; vscreen_write_char(vscreen,  '1', pos);
        pos[0]=(i + 8) % 44 - 4;pos[1]=1; vscreen_write_char(vscreen,  '2', pos);
        pos[0]=(i + 12) % 44- 4;pos[1]=1; vscreen_write_char(vscreen, '3', pos);
        pos[0]=(i + 16) % 44- 4;pos[1]=1; vscreen_write_char(vscreen, '4', pos);
        pos[0]=(i + 20) % 44- 4;pos[1]=1; vscreen_write_char(vscreen, '5', pos);
        pos[0]=(i + 24) % 44- 4;pos[1]=1; vscreen_write_char(vscreen, '6', pos);
        pos[0]=(i + 28) % 44- 4;pos[1]=1; vscreen_write_char(vscreen, '7', pos);
        pos[0]=(i + 32) % 44- 4;pos[1]=1; vscreen_write_char(vscreen, '8', pos);
        pos[0]=(i + 36) % 44- 4;pos[1]=1; vscreen_write_char(vscreen, '9', pos);

        i++;
        // Update the VScreen
        vscreen_update(vscreen);

        // Wait 1/60th of a second: the poor mans frame limiter!
        usleep(6e6/24);
    }

    // Clean up and go home
    vscreen_finalise();
    return 0;
}


