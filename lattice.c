/*======[ FOURIER ]======
 *
 * Description: A test program for VScreen, drawing a Fourier pattern
 *
 * Author: Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "vscreen.h"
 
#define pi 3.14159265

// A pretty-looking Fourier grid that gently wafts through time
void update_buffer (VScreen *vs) {
    float t = vs->time;
    int m = vs->resolution[0];
    int n = vs->resolution[1];
    for(int i=0; i<m; i++) {
        for(int j=0; j<n; j++) {
            vs->buffer[i][j] = 63*(cos(t) * cos((6*pi*i) / m) + 1)
                              +63*(sin(t) * cos((6*pi*j) / n) + 1);
        }
    }
}

int main(int argc, char **argv) {

    // Initialise VScreen and get a new instance
    vscreen_init();

    int resolution[2]={64,48}, scale=10, fullscreen=0;
    VScreen *vscreen = vscreen_new("Fourier Grid", resolution, scale, fullscreen);

    // Start main loop
    while (vscreen_open_screens() > 0) {

        // Calculate the Fourier grid to display
        update_buffer(vscreen);

        // Update the VScreen
        vscreen_update(vscreen);

        // Wait 1/60th of a second: the poor mans frame limiter!
        usleep(1e6/24);
    }

    // Clean up and go home
    vscreen_finalise();
    return 0;
}
