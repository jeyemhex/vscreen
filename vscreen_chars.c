/*======[ vscreen ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2017-11-07
 *
 * This code is distributed under the MIT license.
 */ 
#include <stdlib.h>
#include <stdio.h>

#include "vscreen.h"

// write a character to a position
void vscreen_write_char(VScreen *vs, char c, int pos[2]) {
    switch (c) {
         case ' ': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '!': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,0,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;


        } case '"': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '#': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,255,255,0},{255,0,255,0},{255,255,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '$': {
            int pixels[6][4] = {{0,255,0,0},{255,255,0,0},{0,255,255,0},{255,255,0,0},{0,255,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '%': {
            int pixels[6][4] = {{0,0,0,0},{0,0,255,0},{255,0,0,0},{0,255,0,0},{0,0,255,0},{255,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '&': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,0,255,0},{0,255,0,0},{255,0,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '\'': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,255,0,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '(': {
            int pixels[6][4] = {{0,0,0,0},{0,0,255,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{0,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case ')': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{255,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '*': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{255,0,255,0},{0,255,0,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '+': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,255,0,0},{255,255,255,0},{0,255,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case ',': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{0,255,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '-': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{255,255,255,0},{0,0,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '.': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '/': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{255,255,0,0},{0,255,0,0},{0,255,255,0},{0,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '0': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{255,0,255,0},{255,0,255,0},{255,0,255,0},{0,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

       } case '1': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{255,255,0,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '2': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{255,0,0,0},{0,255,0,0},{0,0,255,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '3': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{0,0,255,0},{0,255,0,0},{0,0,255,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '4': {
            int pixels[6][4] = {{0,0,0,0},{0,0,255,0},{0,0,255,0},{255,255,255,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '5': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{0,0,255,0},{255,255,0,0},{255,0,0,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '6': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{255,0,255,0},{255,255,255,0},{255,0,0,0},{0,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '7': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{255,0,0,0},{0,255,0,0},{0,0,255,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '8': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{255,0,255,0},{255,255,255,0},{255,0,255,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '9': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{0,0,255,0},{255,255,255,0},{255,0,255,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case ':': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,255,0,0},{0,0,0,0},{0,255,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case ';': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{0,255,0,0},{0,0,0,0},{0,255,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '<': {
            int pixels[6][4] = {{0,0,0,0},{0,0,255,0},{0,255,0,0},{255,0,0,0},{0,255,0,0},{0,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '=': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{255,255,255,0},{0,0,0,0},{255,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '>': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{0,255,0,0},{0,0,255,0},{0,255,0,0},{255,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '?': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,0,0,0},{0,255,0,0},{0,0,255,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '@': {
            int pixels[6][4] = {{0,255,255,0},{255,0,0},{255,255,255,0},{255,255,255,0},{255,0,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'A': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,0,255,0},{255,255,255,0},{255,0,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'B': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{255,0,255,0},{255,255,0,0},{255,0,255,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'C': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,0,0,0},{255,0,0,0},{255,0,0,0},{0,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'D': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{255,0,255,0},{255,0,255,0},{255,0,255,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'E': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{255,0,0,0},{255,255,0,0},{255,0,0,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'F': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{255,0,0,0},{255,255,0,0},{255,0,0,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'G': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,0,255,0},{255,255,255,0},{255,0,0,0},{0,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'H': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,0,255,0},{255,255,255,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'I': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'J': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{255,0,255,0},{0,0,255,0},{0,0,255,0},{0,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'K': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,0,255,0},{255,255,0,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'L': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{255,0,0,0},{255,0,0,0},{255,0,0,0},{255,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'M': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,0,255,0},{255,255,255,0},{255,255,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'N': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,255,255,0},{255,255,255,0},{255,255,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'O': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{255,0,255,0},{255,0,255,0},{255,0,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'P': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{255,0,0,0},{255,255,0,0},{255,0,255,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'Q': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,255,255,0},{255,0,255,0},{255,0,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'R': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,255,0,0},{255,0,255,0},{255,0,255,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'S': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{0,0,255,0},{0,255,0,0},{255,0,0,0},{0,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'T': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'U': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,0,255,0},{255,0,255,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'V': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,255,0,0},{255,0,255,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'W': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,255,255,0},{255,255,255,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'X': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,0,255,0},{0,255,0,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'Y': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{255,0,255,0},{255,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'Z': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{255,0,0,0},{0,255,0,0},{0,0,255,0},{255,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '[': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{0,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '\\': {
            int pixels[6][4] = {{0,0,0,0},{0,0,255,0},{0,255,255,0},{0,255,0,0},{255,255,0,0},{255,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case ']': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '^': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{255,0,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '_': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '`': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'a': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{255,0,255,0},{255,0,255,0},{0,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'b': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{255,0,255,0},{255,0,255,0},{255,255,0,0},{255,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'c': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,0,0,0},{255,0,0,0},{0,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'd': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,0,255,0},{255,0,255,0},{0,255,255,0},{0,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'e': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,255,0,0},{255,0,255,0},{0,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'f': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,255,0,0},{255,255,255,0},{0,255,0,0},{0,0,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'g': {
            int pixels[6][4] = {{0,255,0,0},{0,0,255,0},{255,255,255,0},{255,0,255,0},{0,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'h': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,0,255,0},{255,0,255,0},{255,255,0,0},{255,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'i': {
            int pixels[6][4] = {{0,0,0,0},{0,0,255,0},{0,255,0,0},{0,255,0,0},{0,0,0,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'j': {
            int pixels[6][4] = {{255,0,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{0,0,0,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'k': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,255,0,0},{255,255,0,0},{255,0,255,0},{255,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'l': {
            int pixels[6][4] = {{0,0,0,0},{0,0,255,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'm': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,0,255,0},{255,255,255,0},{255,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'n': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{255,0,255,0},{255,0,255,0},{255,255,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'o': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{255,0,255,0},{255,0,255,0},{0,255,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'p': {
            int pixels[6][4] = {{255,0,0,0},{255,255,0,0},{255,0,255,0},{255,0,255,0},{255,255,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'q': {
            int pixels[6][4] = {{0,0,255,0},{0,255,255,0},{255,0,255,0},{255,0,255,0},{0,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'r': {
            int pixels[6][4] = {{0,0,0,0},{255,0,0,0},{255,0,0,0},{255,0,0,0},{0,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 's': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{0,255,255,0},{255,255,0,0},{0,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 't': {
            int pixels[6][4] = {{0,0,0,0},{0,0,255,0},{0,255,0,0},{0,255,0,0},{255,255,255,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'u': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{255,0,255,0},{255,0,255,0},{255,0,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'v': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{255,0,255,0},{255,0,255,0},{255,0,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'w': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{255,255,255,0},{255,255,255,0},{255,0,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'x': {
            int pixels[6][4] = {{0,0,0,0},{255,0,255,0},{0,255,0,0},{0,255,0,0},{255,0,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'y': {
            int pixels[6][4] = {{0,255,0,0},{0,0,255,0},{0,255,255,0},{255,0,255,0},{255,0,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case 'z': {
            int pixels[6][4] = {{0,0,0,0},{255,255,255,0},{0,255,0,0},{0,0,255,0},{255,255,255,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '{': {
            int pixels[6][4] = {{0,0,0,0},{0,255,255,0},{0,255,0,0},{255,255,0,0},{0,255,0,0},{0,255,255,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '|': {
            int pixels[6][4] = {{0,0,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0},{0,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '}': {
            int pixels[6][4] = {{0,0,0,0},{255,255,0,0},{0,255,0,0},{0,255,255,0},{0,255,0,0},{255,255,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } case '~': {
            int pixels[6][4] = {{0,0,0,0},{0,0,0,0},{255,255,0,0},{0,255,255,0},{0,0,0,0},{0,0,0,0}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;

        } default: {
            int pixels[6][4] = {{255,255,255,255},{255,0,0,255},{255,0,0,255},{255,0,0,255},{255,0,0,255},{255,255,255,255}};
            for (int j=0;j<6;j++){
                if ((pos[1] + j) >= vs->resolution[1]) break;
                if ((pos[1] + j) < 0) continue;
                for(int i=0;i<4;i++) {
                    if ((pos[0] + i) >= vs->resolution[0]) break;
                    if ((pos[0] + i) < 0) continue;
                    vs->buffer[pos[0]+i][pos[1]+j] = pixels[j][i];
                }
            }
            break;
        }
    }
}

// write a string to a position
void vscreen_write_string(VScreen *vs, char *str, int len, int pos[2]) {
    int loc_pos[2] = {pos[0], pos[1]};
    for (int i=0; i<len; i++) {
        vscreen_write_char(vs, str[i], loc_pos);
        loc_pos[0] += 4;
    }
}






