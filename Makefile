CC     = gcc
CFLAGS = -O3
LFLAGS = -lglfw -lGL -lm

all: fourier pixel cursor clock mandelbrot numbers font

pixel: pixel.o vscreen.glfw.o

fourier: fourier.o vscreen.glfw.o

mandelbrot: mandelbrot.o vscreen.glfw.o

numbers: numbers.o vscreen.glfw.o vscreen_chars.o

font: font.o vscreen.glfw.o vscreen_chars.o

clock: clock.o vscreen.glfw.o vscreen_chars.o

cursor: cursor.o vscreen.glfw.o

%: %.o
	$(CC) $(CFLAGS) -o $@ $^ $(LFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<
clean:
	rm -f *.o fourier pixel cursor
